// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';

// Deconstruct the  "courseProp" from the "props" object to shorten
export default function CourseCard( {courseProp} ){

    // check to see if the data was passed successfully
    // console.log(props);

    // Deconstruct courseProp properties into their own variable
    const { _id, name, description, price, slots } = courseProp;

    // States 
        // State are used to keep track the information related to individual component.
    // Hooks
        // Special react defiend methods and functions that allows us to do certain tasks in our components.
        // Use the state hook for this component to be able to store its state.
    // Syntax
        // const [stateName, setStateName] = useState(initialStateValue);

    // useState() is a hook that creates states.
        // useState() returns an arra with 2 items:
            // The first item in the array is the state
            // and the second one is the setter function (to change the initial state).
    
    // Array destructuring for the useState()
    // const [count, setCount] = useState(0);
    // console.log(useState(0))


    /*
    ACTIVITY
        Instructions s46 Activity:
        1. Create a seats state in the CourseCard component and set the initial value to 10.
        2. For every enrollment, deduct one to the seats.
        3. If the seats reaches zero do the following:
        - Do not add to the count.
        - Do not deduct to the seats.
        - Show an alert that says No more seats available.
        5. push to git with the commit message of Add activity code - S46.
        6. Add the link in Boodle.

    ACTIVITY SOLUTION
        const [seats, setSeats] = useState(30);

        function enroll(){

            if(seats > 0){
                setCount(count + 1)
                console.log("Enrolees: " + count)

                setSeats(seats - 1)
                console.log("Seats: " + seats)
                return;
            }
            else alert("No more seats available")

        }
    
    
    */
    // const [seats, setSeats] = useState(30);
    // const [isOPen, setIsOPen] = useState(false);

    // function enroll(){

    //     setCount(count + 1);
    //     // console.log("Enrolees: " + count)

    //     setSeats(seats - 1);
    //     // console.log("Seats: " + seats)

    // }

    // function unEnroll(){
    //     setCount(count - 1);
    // }

        
    // // useEffect()
    //     // useEffect allows us to run a task or an effect, the difference is that with useEffect we can manipulate when it will run.
    // // Syntax: 
    //     // useEffect(function, [dependency])

    // // useEffect(() => {
    // //     if(seats === 0){
    // //         alert("No more seats available");
    // //         setIsOPen(true);
    // //     }
    // // }, [seats])


    // If the useEffec() dose not have a dependency array, it will run on the initial render and whenever a state is set by its set function
    // useEffect(() => {
    //     // Runs on every render
    //     console.log("useEffect Render")
    // })

    // If the useEffect() has a dependency array but empty it will only run on initial runder
    // useEffect(() => {
    //     // Runs only on the initial render
    //     console.log("useEffect Render")
    // }, [])

    // If the useEffect() has a dependecy array and there is state or data in it, the useEffect() will run whenever state is updated.
    // useEffect(() => {
    //     // Runs on a initial render.
    //     // And everytime the dependency vlaue will change (seats).
    //     console.log("useEffect Render")
    // }, [seats, count])


    return (
        <Card className="p-3">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description: </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>
		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text>
		            {price}
		        </Card.Text>
                <Card.Text>
		            Slots: {slots}
		        </Card.Text>
		        <Button as={Link} to={`/courses/${_id}`} variant="primary" >Details</Button>
                
		        {/* <Button variant="primary mx-1" onClick={enroll} >Enroll</Button>
		        <Button variant="danger" onClick={unEnroll} >Unenroll</Button> */}
		    </Card.Body>
		</Card>
    );
};