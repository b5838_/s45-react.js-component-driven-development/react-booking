import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout(){
    // localStorage.clear();

    // Consume the UserContext object and destructure it to access.
    const {user, setUser, unsetUser} = useContext(UserContext);

    // Clear the localStorage of the user's information
    unsetUser();
    console.log(user);

    // re-render the page when there are changes to the dependency
    // if no dependency it will re-render the page onclick and initial render
    useEffect( () => {
        // Set the user state back to it's original value.
        setUser({
            id: null,
            isAdmin: null
        })
    })

    return(
        // Redirect back to the login
            <Navigate to="/login"/>
    )

}