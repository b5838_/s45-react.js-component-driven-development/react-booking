// import coursesData from "../data/coursesData";
import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses() {

    // State that will be used to store the courses retrieved from the database
    const [courses, setCourses] = useState([]);

    // Retrieve the courses from the database upon initial render of the Course component
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setCourses(data.map(course => {
                return(
                    <CourseCard key={course._id} courseProp={course} />
                )
            }));
        })
    }, [])

    // Chech if we can access the mock database
    // console.log(coursesData);
    // console.log(coursesData[0]);

    // Props
        // is a shorthand for property since components are considered as object in ReactJS
        // Props is a way to pass the data from the parent to child component.
        // it is a synonymous to the function paramenter.

    // map method

    // const courses = coursesData.map(course => {
    //     return(
    //         <CourseCard key={course.id} courseProp = {course} />
    //     )
    // })

    return(
        <>
            <h1>Courses</h1>
            {courses}
        </>
    );
};