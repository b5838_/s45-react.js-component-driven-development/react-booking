import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import { Container } from 'react-bootstrap';

export default function Home(){
    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Oppurtunities for everyone, everywhere.",
        destination: "/courses",
        label: "Enroll Now"
    }

    return(
        <>
        <Container fluid>
            <Banner data={data}/>
            <Highlights />
        </Container>
        </>
    )
}